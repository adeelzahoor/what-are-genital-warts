Genital Warts are caused by a strain of virus that is known as Human Papilloma Virus (HPV). This strain of virus has various serotypes that lead to either benign or malignant conditions in affected human beings. About 35 types are known to have an affinity to genital sites, but the common serotypes that are responsible for genital warts are the benign serotypes of HPV 6 and 11.

**Mode of transmission**

Genital warts are usually transmitted through sexual contact with an affected individual. Vaginal sex, oral sex, anal sex and even close skin-to-skin contact during sexual intercourse can result in the transmission of genital warts. Vertical transmission from mother to child during childbirth has also been documented in literature, although the occurrence of this is very rare.

**Clinical features**

[Genital Warts]( https://stdauthority.com/genital-warts-hpv) can appear as either solitary or multiple growths, with many taking on a cauliflower-like appearance. Warm, moist, non-hair-bearing skin is generally home to a soft and non-keratinized type of warts, whereas dry and hairy skin provide haven to firm and keratinized types of warts. Rapid growth of warts is usually known to occur during pregnancy or in the presence of a discharge. 

**Common sites**

Common sites of genital warts in men are the sub-preputial area, coronal sulcus, inside the urethral meatus, shaft of the penis, the perineum and the anus. 
Common sites of genital warts in women are the vulva, vagina, cervix, perineum and anus.

**Diagnosis**

The diagnosis of genital warts is generally clinical. The physician or venereologist relies on the characteristic clinical appearance of the genital wart. However, for smaller warts that lack the characteristic appearance, the clinician may sometimes apply acetic acid in order for them to turn white and become conspicuous. 

**Treatment**

Treatment of genital warts is preferably done in the setting of an STD clinic. A primary care physician who detects genital warts should refer the concerned patient to a venereologist. 
The two main modes of therapy of genital warts are chemical treatment and physical treatment.

Chemical treatment involves treatment with 0.5% podofilox solution (podophyllin), 5% 5-fluorouracil cream, 20% podophyllin antimitotic solution, Imiquimod cream and Trichloroacetic acid (TCA). The first three of these should be avoided in pregnant patients. Most of these solutions are applied once weekly, although exceptions do exist.

Physical methods of treatment are cryotherapy, electro-surgery and surgical removal. Of these three methods, cryotherapy is non-toxic, does not need anesthesia and if properly done, does not result in scarring.

As part of treatment, patients should be advised to abstain from sexual intercourse until warts have been completely treated to minimize the risk of infecting partners. Advice on safe sexual practices should also follow hand-in-hand. 

**Additional notes**

In women with genital warts, it is advisable to perform screening for cervical cancer, generally through a pap smear. Cervical cancer, like genital warts, is caused by HPV although more malignant strains of 16, 18, 31 and 33 are involved.
